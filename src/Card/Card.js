import './Card.css';

const Card = props => {

    const suits = {
        diamonds: '♦',
        hearts: '♥',
        clubs: '♣',
        spades: '♠'
    };
    const ranks = {
        'q': 'Q',
        'j': 'J',
        'k': 'K',
        'a': 'A',
        '2': '2',
        '3': '3',
        '4': '4',
        '5': '5',
        '6': '6',
        '7': '7',
        '8': '8',
        '9': '9',
        '10': '10'
    }

    return (
        <div className='cardBox'>
            <div className="card">
                <span className="rank">{ranks[props.rank]}</span>
                <span className="suit">{suits[props.suit]}</span>
                <span className="suitB">{suits[props.suit]}</span>
            </div>
            <div className="card">
                <span className="rank">{ranks[props.rank2]}</span>
                <span className="suit">{suits[props.suit2]}</span>
                <span className="suitB">{suits[props.suit2]}</span>
            </div>
            <div className="card">
                <span className="rank">{ranks[props.rank3]}</span>
                <span className="suit">{suits[props.suit3]}</span>
                <span className="suitB">{suits[props.suit3]}</span>
            </div>
            <div className="card">
                <span className="rank">{ranks[props.rank4]}</span>
                <span className="suit">{suits[props.suit4]}</span>
                <span className="suitB">{suits[props.suit4]}</span>
            </div>
            <div className="card">
                <span className="rank">{ranks[props.rank5]}</span>
                <span className="suit">{suits[props.suit5]}</span>
                <span className="suitB">{suits[props.suit5]}</span>
            </div>
        </div>
    );
};

export default Card;

