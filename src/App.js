import {Component} from "react";
import Card from "./Card/Card";

let random = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1) + min);
}
let array = [];
array.splice( 0, 3);

class CardDeck {
    constructor() {
        this.deck = this.getDeck();
    }
    getDeck = () => {
        const suits = ['hearts', 'diamonds', 'clubs', 'spades'];
        const ranks = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'j', 'q', 'k', 'a'];
        let currentDeck = [];
        for (let rank of ranks) {
            for (let suit of suits) {
                let card = { rank: rank, suit: suit};
                currentDeck.push(card);
            }
        }
        return currentDeck;
    }

    getCard = () => {
        let min = 0;
        let max = 52;
        let card;
        while (card === undefined) {
            let number = random(min, max);
            card = this.deck[number];
            this.deck.splice(number, 1);
        }
        return card;
    }
    getCards = howMany => {
        let cardsArray = [];
        for (let i=0; i < howMany; i++) {
            let card = this.getCard();
            cardsArray.push(card);
        }

        const arr = cardsArray
        let data = new Map();
        for (let obj of arr) {
            data.set(obj.rank, obj);
        }
        const newObj = cardsArray.reduce((a, c) => (Object.values(c).forEach(s => a[s] = (a[s] || 0) +1), a), {});

        const deleteK = delete(newObj["hearts", "diamonds", "clubs", "spades"]);
        for (let key in newObj) {
            if (newObj[key] === 2 ) {
                array.push("/ one pair /");
            }else if (newObj[key] === 3) {
                array.push("/ three of a kind /");
            }else if (newObj["hearts"] || newObj["hearts"] === 3 | newObj["hearts"] === 2) {
                delete newObj["hearts"];
            }else if (newObj["diamonds"] || newObj["diamonds"] === 3 || newObj["diamonds"] === 2) {
                delete newObj["diamonds"];
            }else if (newObj["clubs"] || newObj["clubs"] === 3 || newObj["clubs"] === 2) {
                delete newObj["clubs"];
            }else if (newObj["spades"] || newObj["spades"] === 3 || newObj["spades"] === 2) {
                delete newObj["spades"];
            }else if (newObj["spades"] === 5 || newObj["hearts"] === 5 || newObj["clubs"] === 5 || newObj["diamonds"] === 5) {
                array.push("/ flush /");
            }
        }
        return cardsArray;
    }
}
class App extends Component {
    constructor() {
        super();
        let cardDeck = new CardDeck();
        this.state = {
            display: true,
            cards: cardDeck.getCards(5),
        }
    }

    clickBtn = () => {
        array.splice( 0, 3);
        array.splice( 1, 3);
        array.splice( 2, 3);
        let cardDeck = new CardDeck();
        this.setState({
            display: true,
            cards: cardDeck.getCards(5)
        })
    }

    render() {
        return (
            <div className="box">
                <Card rank={this.state.cards[0].rank} suit={this.state.cards[0].suit} suitB={this.state.cards[0].suit}
                      rank2={this.state.cards[1].rank} suit2={this.state.cards[1].suit} suitB2={this.state.cards[0].suit}
                      rank3={this.state.cards[2].rank} suit3={this.state.cards[2].suit} suitB3={this.state.cards[0].suit}
                      rank4={this.state.cards[3].rank} suit4={this.state.cards[3].suit} suitB4={this.state.cards[0].suit}
                      rank5={this.state.cards[4].rank} suit5={this.state.cards[4].suit} suitB5={this.state.cards[0].suit}/>
                <div className="plus">{array}</div>
                <button className='btn' onClick={this.clickBtn}>Click</button>
            </div>
        );
    }
}

export default App;
